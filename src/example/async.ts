import { Async, Pause } from "./..";

(async () => {
  const mapResult = await Async.map([1,2,3,4], async (v, i) => {
    await Pause(1000);

    return v * 2;
  });
  
  console.log('Finished async map', mapResult);

  await Async.forEach([1,2,3,4], async (v, i) => {
    console.log(i);
    await Pause(1000);
  });
  
  console.log('Finished async forEach one by one');

  await Async.forEachAll([1,2,3,4], async (v, i) => {
    console.log(i);
    await Pause(1000);
  });
  
  console.log('Finished async forEach together');
})();