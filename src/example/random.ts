import { Random } from './..';

const arr = [ 1, 2, 3, 4 ];

for (let i = 0; i < 10; i++) {
  console.log(Random.fromArray(arr));
}
// True random from 1, to 4. Ex: 2


for (let i = 0; i < 10; i++) {
  console.log(Random.number(0, 10));
}
// True random from 0, to 10. Ex: 2

console.log(Random.uuid());
// UUID