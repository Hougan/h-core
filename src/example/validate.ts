import Validate from "../classes/Validate";

Validate({
  foo: '123',
  bar: 123,
  one: null,
  ass: 123
}, {
  foo: String,
  bar: Number,
  one: null,
  ass: Number,
  token: String
});

