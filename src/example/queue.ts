import { Queue } from './..';

const queue = new Queue(3);

queue.push(1);
queue.push(2);
queue.push(3);
queue.push(4);

console.log(queue.tail);
// [ 4, 3, 2 ]

queue.clear();

queue.push(1);

console.log(queue.tail);
// [ 1 ]