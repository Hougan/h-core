class Queue {
  private readonly limit : number;
  public tail: any[];

  constructor(limit: number = 5) {
    this.limit = limit;
    this.tail = [];
  }

  /**
   * Add object to queue
   */
  push(obj : any) {
    this.tail.unshift(obj);

    this.tail.length = this.limit;
  }

  /**
   * Clear tail
   */
  clear() {
    this.tail = [];
  }
}

export default Queue;