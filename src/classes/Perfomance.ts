class Perfomance {
  static async elapsed(callback: () => void , name : string = 'Unknown action') {
    const startTime = Date.now();

    await callback();

    console.log(`${name} was executed for ${Date.now() - startTime} ms`);
  }
}

export default Perfomance;