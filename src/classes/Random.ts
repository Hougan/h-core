import crypto from 'crypto';

class Random {
  static fromArray(array) : any {
    if (!Array.isArray(array)) {
      throw new Error('You should pass ArrayObject to this method!');
    }

    const max = array.length - 1;
    return array[Random.number(0, max)];
  }

  static number(min: number = 0, max: number = 100) : number {
    return crypto.randomInt(min, max + 1);
  }

  static uuid() : string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16).toUpperCase();
    });
  }
}

export default Random;