import chalk from 'chalk';
import fs from 'fs';

class Logger {
  /**
   * Log text
   */
  public static log(text: string, type: string = 'INFO') {
    text = `&${new Date().toLocaleTimeString('ru')}& ${type} | ${text}`;
    
    Logger.fileLog(text);

    const reformat = Logger.reformatText(text);
    console.log(reformat);
  }

  private static reformatText(text: string) : string {
    const colorRegex = /~(gr|r|g|b)(.[^~]*)~/;
    const timeRegex = /\&(.*)&/;

    while (text.match(colorRegex)) {
      const match = text.match(colorRegex);
      if (match == null) {
        break;
      }

      const replaceContent = match[0];

      let color = 'red';
      switch (match[1]) {
        case 'r': {
          color = 'red';
          break;
        }
        case 'b': {
          color = 'blue';
          break;
        }
        case 'g': {
          color = 'green';
          break;
        }
        case 'gr': {
          color = 'grey';
          break;
        }
      }


      text = text.replace(replaceContent, chalk.bold[color](match[2]));
    }

    if (text.includes('INFO')) {
      text = text.replace('INFO', chalk.bgBlue(' INFO '))
    }
    if (text.includes('WARN')) {
      text = text.replace('WARN', chalk.bgGray(' WARN '))
    }
    if (text.includes('ERROR')) {
      text = text.replace('ERROR', chalk.bgRed(' FUCK '))
    }

    while (text.match(timeRegex)) {
      const match = text.match(timeRegex);
      if (match == null) {
        break;
      }

      const replaceContent = match[0];
      text = text.replace(replaceContent, chalk.bold.grey(`[${match[1]}]`));
    }

    return text;
  }

  /**
   * Get file name for now
   */
  private static getFileName() {
    const date = new Date();

    const day = date.getDate();
    const dayNumber = day.toString().length == 1
      ? `0${day}`
      : day;
    
    const month = date.getMonth();
    const monthNumber = month.toString().length == 1
      ? `0${month}`
      : month; 

    return `${dayNumber}.${monthNumber}.${date.getFullYear()}.log`;
  }

  /**
   * Write text to log file
   */
  private static fileLog(text: string) {
    try {
      fs.mkdirSync(`./logs/`, {
        recursive: true
      });
    }
    catch {

    }

    try {
      if (!fs.existsSync(`./logs/${Logger.getFileName()}`)) {
        fs.writeFileSync(`./logs/${Logger.getFileName()}`, '');
      }
    }
    catch {

    }
 
    fs.appendFileSync(`./logs/${Logger.getFileName()}`, text + '\n', {
      flag: 'a'
    });
  }
}

export default Logger;