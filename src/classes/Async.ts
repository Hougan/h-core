class Async {
  static async forEach(arr: any[], callback: (input, index) => Promise<void>) : Promise<void> {
    for (let i = 0, length = arr.length; i < length; i++) {
      await callback(arr[i], i);
    }
  }

  static async forEachAll(arr: any[], callback: (input, index) => Promise<void>) : Promise<void> {
    let promises = arr.map((v, index) => callback(v, index));

    await Promise.all(promises);
  }

  static async map(arr: any[], callback: (input, index) => Promise<any>) : Promise<any[]> {
    let promises = arr.map((v, index) => callback(v, index));

    return await Promise.all(promises);
  }
}

export default Async;