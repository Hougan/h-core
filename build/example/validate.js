"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Validate_1 = __importDefault(require("../classes/Validate"));
Validate_1.default({
    foo: '123',
    bar: 123,
    one: null,
    ass: 123
}, {
    foo: String,
    bar: Number,
    one: null,
    ass: Number,
    token: String
});
