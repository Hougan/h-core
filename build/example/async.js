"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("./..");
(async () => {
    const mapResult = await __1.Async.map([1, 2, 3, 4], async (v, i) => {
        await __1.Pause(1000);
        return v * 2;
    });
    console.log('Finished async map', mapResult);
    await __1.Async.forEach([1, 2, 3, 4], async (v, i) => {
        console.log(i);
        await __1.Pause(1000);
    });
    console.log('Finished async forEach one by one');
    await __1.Async.forEachAll([1, 2, 3, 4], async (v, i) => {
        console.log(i);
        await __1.Pause(1000);
    });
    console.log('Finished async forEach together');
})();
