"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Logger_1 = __importDefault(require("../classes/Logger"));
Logger_1.default.log('It is ~gINFO~ log!');
Logger_1.default.log('It is ~bWARN~ log!', 'WARN');
Logger_1.default.log('It is ~rERROR~ log!', 'ERROR');
