"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Pause_1 = __importDefault(require("../utils/Pause"));
Pause_1.default(1000).then(() => {
    console.log('after 1 sec');
});
