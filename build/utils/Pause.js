"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (delay) => {
    return new Promise((res) => {
        setTimeout(() => {
            res();
        }, delay);
    });
};
