"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (input, shouldBeTypes) => {
    Object.keys(shouldBeTypes).forEach(key => input[key] = input[key] || undefined);
    const keys = Object.keys(input);
    keys.forEach(key => {
        const value = input ? input[key] : null;
        const type = shouldBeTypes[key];
        if (value == null && type == null) {
            return;
        }
        if (value == null || !value.__proto__) {
            if (value && type) {
                throw new Error(`1. Expected '${key}' to be 'null', got '${value}'`);
            }
        }
        if (value == null && type != null) {
            throw new Error(`2. Expected '${key}' to be '${type.name}', got '${value}'`);
        }
        if (value.constructor.name !== type.name && Object.getPrototypeOf(value.constructor).name != type.name) {
            throw new Error(`3. Expected '${key}' to be '${type.name}', got '${value.constructor.name || Object.getPrototypeOf(value.constructor).name}'`);
        }
    });
};
