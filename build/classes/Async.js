"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Async {
    static async forEach(arr, callback) {
        for (let i = 0, length = arr.length; i < length; i++) {
            await callback(arr[i], i);
        }
    }
    static async forEachAll(arr, callback) {
        let promises = arr.map((v, index) => callback(v, index));
        await Promise.all(promises);
    }
    static async map(arr, callback) {
        let promises = arr.map((v, index) => callback(v, index));
        return await Promise.all(promises);
    }
}
exports.default = Async;
