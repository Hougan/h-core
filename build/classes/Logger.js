"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const fs_1 = __importDefault(require("fs"));
class Logger {
    /**
     * Log text
     */
    static log(text, type = 'INFO') {
        text = `&${new Date().toLocaleTimeString('ru')}& ${type} | ${text}`;
        Logger.fileLog(text);
        const reformat = Logger.reformatText(text);
        console.log(reformat);
    }
    static reformatText(text) {
        const colorRegex = /~(gr|r|g|b)(.[^~]*)~/;
        const timeRegex = /\&(.*)&/;
        while (text.match(colorRegex)) {
            const match = text.match(colorRegex);
            if (match == null) {
                break;
            }
            const replaceContent = match[0];
            let color = 'red';
            switch (match[1]) {
                case 'r': {
                    color = 'red';
                    break;
                }
                case 'b': {
                    color = 'blue';
                    break;
                }
                case 'g': {
                    color = 'green';
                    break;
                }
                case 'gr': {
                    color = 'grey';
                    break;
                }
            }
            text = text.replace(replaceContent, chalk_1.default.bold[color](match[2]));
        }
        if (text.includes('INFO')) {
            text = text.replace('INFO', chalk_1.default.bgBlue(' INFO '));
        }
        if (text.includes('WARN')) {
            text = text.replace('WARN', chalk_1.default.bgGray(' WARN '));
        }
        if (text.includes('ERROR')) {
            text = text.replace('ERROR', chalk_1.default.bgRed(' FUCK '));
        }
        while (text.match(timeRegex)) {
            const match = text.match(timeRegex);
            if (match == null) {
                break;
            }
            const replaceContent = match[0];
            text = text.replace(replaceContent, chalk_1.default.bold.grey(`[${match[1]}]`));
        }
        return text;
    }
    /**
     * Get file name for now
     */
    static getFileName() {
        const date = new Date();
        const day = date.getDate();
        const dayNumber = day.toString().length == 1
            ? `0${day}`
            : day;
        const month = date.getMonth();
        const monthNumber = month.toString().length == 1
            ? `0${month}`
            : month;
        return `${dayNumber}.${monthNumber}.${date.getFullYear()}.log`;
    }
    /**
     * Write text to log file
     */
    static fileLog(text) {
        try {
            fs_1.default.mkdirSync(`./logs/`, {
                recursive: true
            });
        }
        catch {
        }
        try {
            if (!fs_1.default.existsSync(`./logs/${Logger.getFileName()}`)) {
                fs_1.default.writeFileSync(`./logs/${Logger.getFileName()}`, '');
            }
        }
        catch {
        }
        fs_1.default.appendFileSync(`./logs/${Logger.getFileName()}`, text + '\n', {
            flag: 'a'
        });
    }
}
exports.default = Logger;
