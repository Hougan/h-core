"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Perfomance {
    static async elapsed(callback, name = 'Unknown action') {
        const startTime = Date.now();
        await callback();
        console.log(`${name} was executed for ${Date.now() - startTime} ms`);
    }
}
exports.default = Perfomance;
