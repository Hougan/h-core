"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Queue {
    constructor(limit = 5) {
        this.limit = limit;
        this.tail = [];
    }
    /**
     * Add object to queue
     */
    push(obj) {
        this.tail.unshift(obj);
        this.tail.length = this.limit;
    }
    /**
     * Clear tail
     */
    clear() {
        this.tail = [];
    }
}
exports.default = Queue;
