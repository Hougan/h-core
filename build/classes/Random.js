"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
class Random {
    static fromArray(array) {
        if (!Array.isArray(array)) {
            throw new Error('You should pass ArrayObject to this method!');
        }
        const max = array.length - 1;
        return array[Random.number(0, max)];
    }
    static number(min = 0, max = 100) {
        return crypto_1.default.randomInt(min, max + 1);
    }
    static uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16).toUpperCase();
        });
    }
}
exports.default = Random;
